module go-template-training

go 1.15

require (
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/acarl005/stripansi v0.0.0-20180116102854-5a71ef0e047d
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/aviddiviner/gin-limit v0.0.0-20170918012823-43b5f79762c1
	github.com/bwmarrin/snowflake v0.3.0
	github.com/caarlos0/env/v6 v6.7.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0
	github.com/go-redis/redis/v8 v8.11.3
	github.com/gookit/color v1.4.2
	github.com/imdario/mergo v0.3.12
	github.com/integralist/go-findroot v0.0.0-20160518114804-ac90681525dc
	github.com/jinzhu/copier v0.3.2
	github.com/joho/godotenv v1.3.0
	github.com/modern-go/reflect2 v1.0.1
	github.com/opentracing/opentracing-go v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2
	github.com/swaggo/gin-swagger v1.3.1
	github.com/swaggo/swag v1.7.1
	github.com/toorop/gin-logrus v0.0.0-20210225092905-2c785434f26f
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	github.com/uniplaces/carbon v0.1.6
	go.mongodb.org/mongo-driver v1.7.2
	go.uber.org/atomic v1.9.0 // indirect
)
