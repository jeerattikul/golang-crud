package main

import (
	"go-template-training/app"
	"go-template-training/config"
	_ "go-template-training/docs"
	"go-template-training/service/util/logs"
	"go-template-training/setup"
)

// @termsOfService http://swagger.io/terms/
// @contact.name the developer
// @contact.email developer@touchtechnologies.co.th
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

func main() {
	// load config
	appConfig := config.New()

	// init log
	log := logs.New(appConfig)

	// init setup
	s := setup.New(appConfig, log)

	// setup jaeger
	closer := s.Jaeger()
	defer func() {
		if err := closer.Close(); err != nil {
			log.Error(err)
		}
	}()

	// setup app route and start service
	conf := &app.Config{appConfig, log, s.Logger()}
	app.New(conf).RegisterRoute().Start()
}
